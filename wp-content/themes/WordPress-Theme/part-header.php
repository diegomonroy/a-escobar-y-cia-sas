<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-right align-middle">
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( 'pse' ); ?>
			</div>
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( 'quote' ); ?>
			</div>
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( 'cart' ); ?>
			</div>
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( 'search' ); ?>
			</div>
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( 'social_media' ); ?>
			</div>
		</div>
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-4 columns text-center">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-8 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->