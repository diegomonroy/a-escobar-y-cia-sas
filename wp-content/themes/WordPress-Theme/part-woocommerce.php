<?php
$title = 'PRODUCTOS';
$column_1 = 'medium-3';
$column_2 = 'medium-9';
/* Tienda */
$category_1_id = 74;
$category_1 = 'tienda';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	/* if ( ! is_product() ) { */
		$title = 'TIENDA';
		$column_1 = 'hide';
		$column_2 = '';
	/* } */
}
?>
<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row collapse expanded main_title">
			<div class="small-12 columns">
				<h1 class="text-center"><?php echo $title; ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="small-12 <?php echo $column_1; ?> columns">
				<?php dynamic_sidebar( 'left' ); ?>
			</div>
			<div class="small-12 <?php echo $column_2; ?> columns">
				<div class="woocommerce">
					<?php
					if ( is_shop() && ! is_search() ) :
						get_template_part( 'part', 'block-2' );
					elseif ( is_search() ) :
						woocommerce_content();
					else :
						woocommerce_content();
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->