<!DOCTYPE html>
<html class="no-js" lang="<?php echo get_locale(); ?>">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="l6OO1WI8Qq7O5mXyBn3h2s71JBWbnHyM8dIJ2avypUs">
		<title><?php bloginfo(title); ?></title>
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo site_url(); ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php bloginfo(name); ?>">
		<meta property="og:description" content="<?php bloginfo(description); ?>">
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/build/logo_ogp.png">
		<link rel="image_src" href="<?php echo get_template_directory_uri(); ?>/build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<?php wp_head(); ?>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/build/favicon.ico">
		<!-- Begin Google Analytics -->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158120484-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-158120484-1');
		</script>
		<!-- Google tag (gtag.js) -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-2R73TW9QL7"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'G-2R73TW9QL7');
		</script>
		<!-- End Google Analytics -->
		<!-- Begin Google Ads -->
		<!-- Google tag (gtag.js) -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=AW-668017337"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'AW-668017337');
		</script>
		<!-- Event snippet for Whatsapp conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
		<script> function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-668017337/nCOFCOrG5fcBELnFxL4C', 'event_callback': callback }); return false; } </script>
		<?php if ( is_page( array( 'contacto' ) ) ) : ?>
		<!-- Event snippet for FORM CONTACT conversion page -->
		<!-- https://ontopmarketing.agency/contact-form-7-conversions-in-google-adwords/ -->
		<script>
			document.addEventListener( 'wpcf7mailsent', function ( event ) {
				gtag('event', 'conversion', {'send_to': 'AW-668017337/DemLCOu9m-MYELnFxL4C'});
			}, false );
		</script>
		<?php endif; ?>
		<!-- End Google Ads -->
	</head>
	<body>
		<?php get_template_part( 'part', 'header' ); ?>