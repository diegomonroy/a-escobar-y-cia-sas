<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/foundation/js/vendor/jquery.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/foundation/js/vendor/what-input.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/wow/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
			'product-menu' => __( 'Product Menu' ),
			'servicios-menu' => __( 'Servicios Menu' ),
			'quienes-somos-menu' => __( 'Quiénes Somos Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search',
			'id' => 'search',
			'before_widget' => '<div class="moduletable_to3">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Social Media',
			'id' => 'social_media',
			'before_widget' => '<div class="moduletable_to4 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Quote',
			'id' => 'quote',
			'before_widget' => '<div class="moduletable_to5 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Cart',
			'id' => 'cart',
			'before_widget' => '<div class="moduletable_to6 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'PSE',
			'id' => 'pse',
			'before_widget' => '<div class="moduletable_to7 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Contacto',
			'id' => 'banner_contacto',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left',
			'id' => 'left',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 5',
			'id' => 'block_5',
			'before_widget' => '<div class="moduletable_b51">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 6',
			'id' => 'block_6',
			'before_widget' => '<div class="moduletable_b61">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Functions to declare WooCommerce image sizes
 */
function woocommerce_catalog_image( $size ) {
	return array(
		'width' => 220,
		'height' => 220,
		'crop' => 0,
	);
}
add_filter( 'woocommerce_get_image_size_thumbnail', 'woocommerce_catalog_image' );

function woocommerce_single_image( $size ) {
	return array(
		'width' => 600,
		'height' => 600,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_single', 'woocommerce_single_image' );

function woocommerce_gallery_image( $size ) {
	return array(
		'width' => 200,
		'height' => 200,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'woocommerce_gallery_image' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 8;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Custom hooks in WooCommerce
 */

function custom_open_loop_product() {
	echo '<div class="row align-middle thumbnail-loop"><div class="columns">';
}
add_action( 'woocommerce_before_subcategory_title', 'custom_open_loop_product', 1 );
add_action( 'woocommerce_before_shop_loop_item_title', 'custom_open_loop_product', 1 );

function custom_close_loop_product() {
	echo '</div></div>';
}
add_action( 'woocommerce_before_subcategory_title', 'custom_close_loop_product', 40 );
add_action( 'woocommerce_before_shop_loop_item_title', 'custom_close_loop_product', 40 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function custom_description() {
	echo '<div class="logos">' . get_field( 'logos' ) . '</div>';
}
add_action( 'woocommerce_single_product_summary', 'custom_description', 10 );

function custom_clear() {
	$tab_2 = '';
	$tab_2_content = '';
	if ( get_field( 'ficha_tecnica' ) ) :
		$tab_2 = '<li class="tabs-title"><a data-tabs-target="panel2" href="#panel2">Ficha Técnica</a></li>';
		$tab_2_content = '<div class="tabs-panel" id="panel2">' . get_field( 'ficha_tecnica' ) . '</div>';
	endif;
	$tab_3 = '';
	$tab_3_content = '';
	if ( get_field( 'formulario' ) ) :
		$tab_3 = '<li class="tabs-title"><a data-tabs-target="panel3" href="#panel3">Formulario</a></li>';
		$tab_3_content = '<div class="tabs-panel" id="panel3">' . get_field( 'formulario' ) . '</div>';
	endif;
	echo '
		<div class="clear"></div>
		<ul class="tabs" data-tabs id="product-tabs">
			<li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Descripción</a></li>
			' . $tab_2 . '
			' . $tab_3 . '
		</ul>
		<div class="tabs-content" data-tabs-content="product-tabs">
			<div class="tabs-panel is-active" id="panel1">
	';
	the_content();
	echo '
			</div>
			' . $tab_2_content . '
			' . $tab_3_content . '
		</div>
		<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>
	';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_clear', 10 );

/*
 * Custom shortcode to Wompi form
 */
function shortcode_wompi() {
	$link = 'https://checkout.wompi.co/p/';
	$publickey = 'pub_prod_aP5FIfiTMCMQPxHuyWeiNgwdjB4Hvp6F';
	$currency = 'COP';
	$date = date( 'Y-m-d H:i:s' );
	$reference = 'AE ' . $date;
	$html = '
		<script>
			jQuery( document ).ready(function() {
				jQuery( "#amount" ).on( "change", function() {
					var amount = this.value;
					var chain = amount + 0 + 0;
					jQuery( "#wompi" ).append( "<input type=\"hidden\" name=\"amount-in-cents\" value=\"" + chain + "\">" );
				});
			});
		</script>
		<div>
			<h6 class="text-center">NUESTROS PAGOS SE REALIZAN POR MEDIO DE WOMPI</h6>
			<p class="text-center">Por favor ingrese el valor a pagar (Ej: 100000):</p>
			<form id="wompi" action="' . $link . '" method="GET">
				<input type="number" id="amount" placeholder="Valor" min="0" required>
				<input type="hidden" name="public-key" value="' . $publickey . '">
				<input type="hidden" name="currency" value="' . $currency . '">
				<input type="hidden" name="reference" value="' . $reference . '">
				<div class="text-center"><input type="submit" name="submit" value="Pagar" class="button"></div>
			</form>
		</div>
	';
	return $html;
}
add_shortcode( 'sas_wompi', 'shortcode_wompi' );